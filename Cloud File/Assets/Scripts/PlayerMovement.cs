﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

    NavMeshAgent agent;
    public bool canClick;
    public bool playingDead;

    public DisplayDialog displayDialog;

    // Use this for initialization
    void Start()
    {
        displayDialog = FindObjectOfType<DisplayDialog>();
        agent = GetComponent<NavMeshAgent>();
        canClick = true;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && GameStateManager.gameState == GameStateManager.States.ingame)
            {
                if (playingDead)
                {                    
                    canClick = true;
                    playingDead = false;
                }
                else
                {                   
                    canClick = false;
                    playingDead = true;
                }
            }
        if (canClick)
        {            
            /*if(!displayDialog.msgFinished)
            {*/
                if (Input.GetMouseButtonDown(0))
                {

                    // ScreenPointToRay() takes a location on the screen
                    // and returns a ray perpendicular to the viewport
                    // starting from that location
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    // Note that "11" represents the number of the "ground"
                    // layer in my project. It might be different in yours!
                    LayerMask mask = 1 << 8;

                    // Cast the ray and look for a collision
                    if (Physics.Raycast(ray, out hit, 200, mask))
                    {
                        // If we detect a collision with the ground, 
                        // tell the agent to move to that location
                        agent.destination = hit.point;
                    }
                }

                for (int i = 0; i < Input.touchCount; ++i)
                {
                    if (Input.GetTouch(i).phase.Equals(TouchPhase.Began))
                    {
                        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                        RaycastHit hit;
                        // Note that "11" represents the number of the "ground"
                        // layer in my project. It might be different in yours!
                        LayerMask mask = 1 << 8;

                        // Cast the ray and look for a collision
                        if (Physics.Raycast(ray, out hit, 200, mask))
                        {
                            // If we detect a collision with the ground, 
                            // tell the agent to move to that location
                            agent.destination = hit.point;
                        }
                    }
                }
            /*}*/
            
        }
        

        if (agent.remainingDistance <= float.Epsilon)
        {
            if (!playingDead)
            {
                //Arrived
                canClick = true;
            }            
        }

    }

    public void SwitchTarget(Transform targ)
    {
        agent.destination = targ.position;
    }
}

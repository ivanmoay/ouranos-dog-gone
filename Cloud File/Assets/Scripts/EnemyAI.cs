﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {

    public Transform[] waypoints;
    NavMeshAgent agent;

    int i;

	// Use this for initialization
	void Start () {
        agent = GetComponent<NavMeshAgent>();
        i = 0;
        //Debug.Log(waypoints.Length);
	}
	
	// Update is called once per frame
	void Update () {
        agent.destination = waypoints[i].position;
        if (!agent.pathPending)
        {
            if (agent.remainingDistance <= agent.stoppingDistance)
            {
                if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
                {
                    i++;                    
                    if (i > waypoints.Length - 1)
                    {
                        i = 0;
                    }
                    agent.destination = waypoints[i].position;
                }
            }
        }
	}
}

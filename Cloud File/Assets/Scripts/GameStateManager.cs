﻿using UnityEngine;
using System.Collections;

public class GameStateManager : MonoBehaviour {
    public static GameStateManager Instance;

    public enum States
    {
        paused,
        ingame,
        playdead,
        dialog,
        win,
        lose
    }

    public static States gameState;

    public static PlayerMovement playerMovement = new PlayerMovement();

    void Start()
    {
        Instance = this;
        gameState = States.ingame;
        playerMovement = GameObject.FindObjectOfType<PlayerMovement>();
    }
	
	
	void Update () {
        print(gameState);
        if (gameState == States.paused)
        {
            paused();
        }
        else if (gameState == States.ingame)
        {
            ingame();
        }
        else if (gameState == States.playdead)
        {
            playdead();
        }
        else if (gameState == States.dialog)
        {
            dialog();
        }
        else if (gameState == States.win)
        {
            win();
        }
        else if (gameState == States.lose)
        {
            lose();
        }
	}

    static void paused()
    {

    }

    static void ingame()
    {
        if (!playerMovement.playingDead)
        {
            playerMovement.canClick = true;
        }        
        if (GameManager.controlsEnabled)
        {
            GameManager.EnableControl();
        }        
    }

    static void playdead()
    {

    }
       
    static void dialog()
    {
        playerMovement.canClick = false;
        if (!GameManager.controlsEnabled)
        {
            GameManager.DisableControl();
        }   
        
    }

    static void win()
    {

    }

    static void lose()
    {

    }

}

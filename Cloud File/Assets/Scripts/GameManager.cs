﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	public static GameManager Instance;

	public Behaviour[] controlsToDisableOnDialog;

    public Camera[] cameras;

    public Camera mainCamera;

    public static bool controlsEnabled = true;
    
	// Use this for initialization
	void Start () {
		Instance = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public static void ReturnToMainCamera()
    {
        foreach (Camera c in Instance.cameras)
        {
            //c.enabled = false;
            c.gameObject.SetActive(false);
        }

        Instance.mainCamera.gameObject.SetActive(true);
    }

    public static void SwitchCamera(Camera camera)
    {
        foreach (Camera c in Instance.cameras)
        {
            //c.enabled = false;
            c.gameObject.SetActive(false);
        }
        camera.gameObject.SetActive(true);
    }

	public static void DisableControl(){
        controlsEnabled = false;
		foreach(Behaviour bev in Instance.controlsToDisableOnDialog){
			bev.enabled = false ;
		}
	}
	public static void EnableControl(){
        controlsEnabled = true;
		foreach(Behaviour bev in Instance.controlsToDisableOnDialog){
			bev.enabled = true;
		}
	}
}

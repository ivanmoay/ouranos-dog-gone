﻿using UnityEngine;
using System.Collections;

public class ItemChecker : MonoBehaviour {

	public DisplayDialog displayDialog;
    public Transform RetreatTo;
    public string itemToCheck;
    public Camera camera1;
    public Camera camera2;

	// Use this for initialization
	void Start () {
	    displayDialog = FindObjectOfType<DisplayDialog>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
        	if(other.GetComponent<Inventory>().HasItem(itemToCheck))
        	{
				//other.GetComponent<PlayerMovement>().canClick = false;
                GameManager.SwitchCamera(camera1);
	            displayDialog.ShowMessage("WAIT!...carefull not to get spotted");
	            Destroy(gameObject);
    		}else
    		{
    			//other.GetComponent<PlayerMovement>().canClick = false;
                GameManager.SwitchCamera(camera2);
	            other.GetComponent<PlayerMovement>().SwitchTarget(RetreatTo);
	            displayDialog.ShowMessage("Arent you forgetting something?");
    		}
            
        }
    }
}

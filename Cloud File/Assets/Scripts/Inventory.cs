﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour {

	List<string> items= new List<string>();

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void AddToInventory(string itemToAdd){
		items.Add(itemToAdd);
	}

	public bool HasItem(string itemToCheck){

		foreach (string item in items) 
		{
		    if(item == itemToCheck)
		    {
		    	return true;
		    }
		}


		return false;
	}
}

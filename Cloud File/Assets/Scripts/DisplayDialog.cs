﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DisplayDialog : MonoBehaviour {

	public GameObject messageBox;
    public Text messageBoxText;
    public float characterDelay = 0.1f;

    public string message;

    private string currentMessage;

    void Start()
    {
        //ShowMessage();
    }

    public void ShowMessage(string tMsg)
    {
        GameStateManager.gameState = GameStateManager.States.dialog;
    	message = tMsg;
        currentMessage = tMsg;
        messageBox.SetActive(true);
        StopAllCoroutines();
        StartCoroutine(TypeMessage());
    }

    IEnumerator TypeMessage()
    {
        messageBoxText.text = "";

        foreach(char c in message)
        {
            yield return new WaitForSeconds(characterDelay);
            messageBoxText.text += c;
            if(messageBox.GetComponent<AudioSource>() != null)
            {
                messageBox.GetComponent<AudioSource>().Play();
            }
        }
    }

    bool clicked = false;
    void Update()
    {
        if (GameStateManager.gameState == GameStateManager.States.dialog)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                StopAllCoroutines();
                //print(messageBoxText.text.Length + " : " + currentMessage.Length);
                if (messageBoxText.text.Length == currentMessage.Length)
                {
                    messageBox.SetActive(false);
                    GameManager.ReturnToMainCamera();
                    GameStateManager.gameState = GameStateManager.States.ingame;
                }
                messageBoxText.text = currentMessage;
            }
        }
    }
}

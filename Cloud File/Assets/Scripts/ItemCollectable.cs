﻿using UnityEngine;
using System.Collections;

public class ItemCollectable : MonoBehaviour {

	public string name;

	void OnTriggerEnter(Collider other) {
		if(other.gameObject.tag == "Player"){
			 other.GetComponent<Inventory>().AddToInventory(name);
			 Destroy(gameObject);
		}
       
    }


}

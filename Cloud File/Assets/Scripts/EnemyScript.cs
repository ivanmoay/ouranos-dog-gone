﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour {

    /*PlayerHealth ph;

    public int damageToGive;

    public Transform player;
    NavMeshAgent agent;

    public string state;

    public float followTime = 10;

	// Use this for initialization
	void Start () {
        ph = FindObjectOfType<PlayerHealth>();
        agent = GetComponent<NavMeshAgent>();

        state = "patrol";

	}
	
	// Update is called once per frame
	void Update () {

        if (state == "patrol")
        {
            if (this.agent.pathPending)
            {
                return;
            }                
            okGO();
            followTime += Time.deltaTime;
            if (followTime >= 10)
            {
                followTime = 10;
            }
        }
        if (state == "follow" && followTime > 0)
        {
            agent.destination = player.position;
            followTime -= Time.deltaTime;
            if (followTime <= 0)
            {
                state = "patrol";
            }
        }
        
	}

    protected virtual void okGO()
    {
        if (this.agent.desiredVelocity.magnitude == 0)
        {
            this.agent.SetDestination(GetRandomTargetPoint());
        }            
    }

    private Vector3 minBoundsPoint;
    private Vector3 maxBoundsPoint;
    private float boundsSize = float.NegativeInfinity;
    private Vector3 GetRandomTargetPoint()
    {
        if (boundsSize < 0)
        {
            minBoundsPoint = Vector3.one * float.PositiveInfinity;
            maxBoundsPoint = -minBoundsPoint;
            var vertices = NavMesh.CalculateTriangulation().vertices;
            foreach (var point in vertices)
            {
                if (minBoundsPoint.x > point.x)
                    minBoundsPoint = new Vector3(point.x, minBoundsPoint.y, minBoundsPoint.z);
                if (minBoundsPoint.y > point.y)
                    minBoundsPoint = new Vector3(minBoundsPoint.x, point.y, minBoundsPoint.z);
                if (minBoundsPoint.z > point.z)
                    minBoundsPoint = new Vector3(minBoundsPoint.x, minBoundsPoint.y, point.z);
                if (maxBoundsPoint.x < point.x)
                    maxBoundsPoint = new Vector3(point.x, maxBoundsPoint.y, maxBoundsPoint.z);
                if (maxBoundsPoint.y < point.y)
                    maxBoundsPoint = new Vector3(maxBoundsPoint.x, point.y, maxBoundsPoint.z);
                if (maxBoundsPoint.z < point.z)
                    maxBoundsPoint = new Vector3(maxBoundsPoint.x, maxBoundsPoint.y, point.z);
            }
            boundsSize = Vector3.Distance(minBoundsPoint, maxBoundsPoint);
        }
        var randomPoint = new Vector3(
            Random.Range(minBoundsPoint.x, maxBoundsPoint.x),
            Random.Range(minBoundsPoint.y, maxBoundsPoint.y),
            Random.Range(minBoundsPoint.z, maxBoundsPoint.z)
        );
        NavMeshHit hit;
        NavMesh.SamplePosition(randomPoint, out hit, boundsSize, 1);
        return hit.position;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            //Destroy(other.gameObject);
            InvokeRepeating("Damage", 0, 0.5f);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            CancelInvoke("Damage");
        }
    }

    public void Damage()
    {
        //ph.health -= damageToGive;
        ph.TakeDamage(damageToGive);
        //Debug.Log("ouch");
        //ph.AddjustCurrentHealth(-damageToGive);
    }*/
}

﻿using UnityEngine;
using System.Collections;

public class Goal : MonoBehaviour {

    public string[] scenes;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Application.LoadLevel(scenes[Random.Range(0,scenes.Length)]);

        }
    }
}

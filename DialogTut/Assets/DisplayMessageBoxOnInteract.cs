﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DisplayMessageBoxOnInteract : MonoBehaviour {

    public GameObject messageBox;
    public Text messageBoxText;
    public float characterDelay = 0.1f;
    public AudioClip typingAudio;

    public string message;

    void Start()
    {
        Interact();
    }

    void Interact()
    {
        messageBox.SetActive(true);

        StartCoroutine(TypeMessage());
    }

    IEnumerator TypeMessage()
    {
        messageBoxText.text = "";


        foreach(char c in message)
        {
            yield return new WaitForSeconds(characterDelay);
            messageBoxText.text += c;
        }
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            messageBox.SetActive(false);
        }
    }
}
